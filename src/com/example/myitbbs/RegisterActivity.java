package com.example.myitbbs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.globe.Globe;
import com.example.myitbbs.model.User;
import com.example.myitbbs.util.HttpUtil;

public class RegisterActivity extends Activity {

	private EditText etName;
	private EditText etPassWord;
	private EditText etPassWordToo;
	private EditText etEmail;

	
	private Button btnRegister;
	String userPassWord;
	String userPassWordToo;
	String userEmail;
	String userName;
	
	MyHandler myHandler;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		etName = (EditText) findViewById(R.id.etName);
		etPassWord = (EditText) findViewById(R.id.etPassWord);
		etPassWordToo = (EditText) findViewById(R.id.etPassWordToo);
		etEmail = (EditText) findViewById(R.id.etEmail);

		btnRegister = (Button) findViewById(R.id.btnRegister);
		myHandler = new MyHandler();

		btnRegister.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				userPassWord = etPassWord.getText().toString();
				userPassWordToo = etPassWordToo.getText().toString();
				userEmail = etEmail.getText().toString();
				
				if (!userPassWord.equals(userPassWordToo)) {
					Toast.makeText(RegisterActivity.this, "两次密码输入不一致",
							Toast.LENGTH_SHORT).show();
					etPassWord.setText("");
					etPassWordToo.setText("");
				} else if (userEmail == null || userEmail == "") {
					Toast.makeText(RegisterActivity.this, "请输入Email",
							Toast.LENGTH_SHORT).show();
				}
				else {					
					userName = etName.getText().toString();
					register(userName, userPassWordToo, userEmail);
				}
			}
		});
	}

	private void register(final String userName, final String userPassWord,
			final String userEmail) {
		new Thread() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Message msg = new Message();
				try {
					String result = HttpUtil.getRequest(MyConstants.NewURL
							+ "RegisterActionphoneRegister?userName="
							+ userName + "&userPassword=" + userPassWord
							+ "&userEmail=" + userEmail
							);
					
					if (result.equals("#<registerSuccess>\r\n")) {//服务器发过来的消息会自动补上\r\n
						// 注册成功
						msg.what = 1;
						RegisterActivity.this.myHandler.sendMessage(msg);
					} else if (result.equals("#<registerUnSuccess>\r\n")) {
						// 注册失败用户名重复
						msg.what = 2;
						RegisterActivity.this.myHandler.sendMessage(msg);
					} else {
						// 发生网络错误
						msg.what = 3;
						RegisterActivity.this.myHandler.sendMessage(msg);
					}
				} catch (Exception e) {
					// TODO: handle exception
					msg.what = 3;
					RegisterActivity.this.myHandler.sendMessage(msg);
				}
			}
		}.start();
	}

	class MyHandler extends Handler {
		public MyHandler() {
		}

		public MyHandler(Looper L) {
			super(L);
		}

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			if (msg.what == 1) {
				User user = new User();
				user.setEmail(userEmail);
				user.setName(userName);
				user.setPassWord(userPassWord);
				Globe.user = user;
				Toast.makeText(RegisterActivity.this, "注册成功",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent();
				intent.setClass(RegisterActivity.this, MainActivity.class);
				startActivity(intent);
			} else if (msg.what == 2) {
				Toast.makeText(RegisterActivity.this, "注册失败用户名重复",
						Toast.LENGTH_SHORT).show();
				RegisterActivity.this.etName.setText("");
			} else if (msg.what == 3) {
				Toast.makeText(RegisterActivity.this, "发生网络错误",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

}
