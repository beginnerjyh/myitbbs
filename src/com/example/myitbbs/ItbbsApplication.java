package com.example.myitbbs;

import com.example.myitbbs.http.AsyncHttpClient;

import android.app.Application;

public class ItbbsApplication extends Application {
	private static AsyncHttpClient client = null;

	/**
	 * 使用之前先初始化
	 */
	public AsyncHttpClient getAsyncHttprequest() {
		if (client == null) {
			client = new AsyncHttpClient();
		}
		return client;
	}

}
