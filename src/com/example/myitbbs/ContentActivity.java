package com.example.myitbbs;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings.Global;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dialog.LoginDialog;
import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.globe.Globe;
import com.example.myitbbs.model.Comment;
import com.example.myitbbs.util.GsonUtil;
import com.example.myitbbs.util.HttpUtil;
import com.example.myitbbs.util.StringUtil;
import com.google.gson.reflect.TypeToken;

public class ContentActivity extends Activity {
	private TextView title;
	private TextView author;
	private TextView time;
	private TextView content;
	private Button back;
	private Button comment;
	private TextView showTitle;
	private MyHandler myHandler;
	private List<Comment> lvComment;
	private int topicId = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_content);

		// 初始化显示组件
		showTitle = (TextView) findViewById(R.id.title_text);
		title = (TextView) findViewById(R.id.content_title);
		author = (TextView) findViewById(R.id.content_author);
		time = (TextView) findViewById(R.id.content_time);
		content = (TextView) findViewById(R.id.content_content);
		back = (Button) findViewById(R.id.title_back);
		comment = (Button) findViewById(R.id.title_comment);

		String titleStr = getIntent().getExtras().getString("title");
		String authorStr = getIntent().getExtras().getString("author");
		String timeStr = getIntent().getExtras().getString("time");
		String typeStr = getIntent().getExtras().getString("type");
		String contentStr = getIntent().getExtras().getString("content");

		myHandler = new MyHandler();

		showTitle.setText(typeStr);
		title.setText(titleStr);
		author.setText(authorStr);
		time.setText(timeStr);
		content.setText(StringUtil.encode(contentStr));

		back.setVisibility(View.VISIBLE);
		back.setOnClickListener(new MyOnClickListener());		
		
		comment.setText("评论");
		try {
			topicId = getIntent().getIntExtra("topicId", -1);
			if (topicId != -1) {
				comment.setVisibility(View.VISIBLE);
				loadComment(topicId);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		comment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Globe.user != null) {
					Intent intent = new Intent(ContentActivity.this,
							CommentActivity.class);
					intent.putExtra("topicId", topicId);
					startActivity(intent);
				} else {
					Toast toast = Toast.makeText(ContentActivity.this, "您还未登录，请先登录",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					Intent intent = new Intent(ContentActivity.this, LoginDialog.class);
					startActivity(intent);
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_content, menu);
		return true;
	}

	class MyOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			ContentActivity.this.finish();
		}
	}

	class MyHandler extends Handler {

		public MyHandler() {
		}

		public MyHandler(Looper L) {
			super(L);
		}

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if (msg.what == 1) {
				int lvComentSize = lvComment.size();
				LinearLayout ll = (LinearLayout) findViewById(R.id.ll);
				for (int i = 0; i < lvComentSize; i++) {
					TextView tv1 = new TextView(ContentActivity.this);
					int l = i + 1;
					tv1.setText("#" + l + "楼" + "  评论人："
							+ lvComment.get(i).getUser().getName() + "  评论时间："
							+ lvComment.get(i).getDate());
					tv1.setTextSize(10);
					ll.addView(tv1);
					TextView tv = new TextView(ContentActivity.this);
					tv.setText(lvComment.get(i).getContent());
					tv.setTextColor(Color.BLACK);
					ll.addView(tv);
				}
			}
		}
	}

	private void loadComment(final int topicId) {
		new Thread() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Message msg = new Message();
				try {
					String result = HttpUtil.getRequest(MyConstants.NewURL
							+ "PageInfoActionphoneComment?topicId=" + topicId);
					System.out.println("1->" + result);
					if (result != null) {
						lvComment = GsonUtil.getGson().fromJson(result,
								new TypeToken<List<Comment>>() {
								}.getType());
						msg.what = 1;
					} else {
						msg.what = 2;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					msg.what = 2;
				} finally {
					ContentActivity.this.myHandler.sendMessage(msg);
				}
			}
		}.start();
	}

}
