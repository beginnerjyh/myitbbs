package com.example.myitbbs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.globe.Globe;
import com.example.myitbbs.util.HttpUtil;

public class CommentActivity extends Activity {

	private Button btnComment;
	private EditText etComment;
	MyHandler myHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comment);
		btnComment = (Button) findViewById(R.id.btnComment);
		etComment = (EditText) findViewById(R.id.etComment);
		myHandler = new MyHandler();

		btnComment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String msgComment = etComment.getText().toString();
				if (msgComment != null && msgComment != "") {
					int topicId = getIntent().getIntExtra("topicId", -1);
					if (topicId != -1) {
						comment(msgComment, topicId);
					} else {
						Toast toast = Toast.makeText(CommentActivity.this,
								"发生未知错误", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}
				} else {
					Toast toast = Toast.makeText(CommentActivity.this,
							"回复内容不能为空", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_comment, menu);
		return true;
	}

	private void comment(final String msgComment, final int topicId) {
		new Thread() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Message msg = new Message();
				try {
					String result = HttpUtil.getRequest(MyConstants.NewURL
							+ "UserActionphoneWriteComment?phone_topicId="
							+ topicId + "&phone_userId=" + Globe.user.getId()
							+ "&phone_comment=" + msgComment);
					if(result.equals("#<replySuccess>\r\n")){
						msg.what = 1;
					} else if(result.equals("#<replyUnSuccess>\r\n")){
						msg.what = 2;
					}
				} catch (Exception e) {
					// TODO: handle exception
					msg.what = 3;
				} finally {
					CommentActivity.this.myHandler.sendMessage(msg);
				}
			}
		}.start();
	}
	class MyHandler extends Handler{
		public MyHandler() {
		}

		public MyHandler(Looper L) {
			super(L);
		}
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if(msg.what == 1){
				Intent intent = new Intent(CommentActivity.this, MainActivity.class);
				Toast toast = Toast.makeText(CommentActivity.this,
						"评论成功", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				startActivity(intent);
			} else {
				Toast toast = Toast.makeText(CommentActivity.this,
						"发生未知错误，评论失败", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
		}
		
	}

}
