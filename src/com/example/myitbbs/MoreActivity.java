package com.example.myitbbs;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dialog.LoginDialog;
import com.example.myitbbs.globe.Globe;

public class MoreActivity extends Activity {
	TextView titleText;
	ListView lvMore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more);
		lvMore = (ListView) findViewById(R.id.lvMore);

		// 设置标题
		titleText = (TextView) findViewById(R.id.title_text);
		titleText.setText(R.string.title_more);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_more, menu);
		return true;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MyBaseAdapter myBaseAdapter = new MyBaseAdapter();
		lvMore.setAdapter(myBaseAdapter);
		lvMore.setOnItemClickListener(new MyOnItemClickListener());
	}

	class MyBaseAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView textView = new TextView(MoreActivity.this);
			textView.setTextSize(30);
			textView.setTextColor(Color.BLACK);
			switch (position) {
			case 0:
				textView.setText("关于");
				break;
			case 1:
				textView.setText("设置");
				break;
			case 2:
				if (Globe.user != null) {
					textView.setText("注销");
				} else {
					textView.setText("登陆");
				}
				break;
			default:
				break;
			}
			return textView;
		}

	}

	class MyOnItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			switch (position) {
			case 0:
				// 跳转关于
				break;
			case 1:
				// 跳转设置
				break;
			case 2:
				// 跳转登陆或注销
				if (Globe.user != null) {
					Globe.user = null;
					Toast toast = Toast.makeText(MoreActivity.this, "已成功注销",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					Intent intent = new Intent(MoreActivity.this,
							MainActivity.class);
					startActivity(intent);
				} else {
					Intent intent = new Intent(MoreActivity.this,
							LoginDialog.class);
					startActivity(intent);
				}
				break;
			default:
				break;
			}
		}

	}

}
