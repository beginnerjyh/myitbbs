package com.example.myitbbs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.manage.DisplayManager;
import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.globe.Globe;
import com.example.myitbbs.model.User;
import com.example.myitbbs.util.GsonUtil;
import com.example.myitbbs.util.HttpUtil;

public class LoginActivity extends Activity {

	private EditText etUserName = null;
	private EditText etPassWord = null;
	private Button btnLogin = null;
	private Button btnRButton = null;
	private User user = null;
	private DisplayManager displayManager;
	private View view;
	private int viewwidth;
	private int viewheight;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		displayManager = DisplayManager.getInstance();
		// view = LayoutInflater.from(LoginActivity.this).inflate(
		// R.layout.activity_login, null);
		// viewwidth = CommonUtil.Px2Dp(this,
		// displayManager.getDisplayWidth(this) * 3 / 4);
		// viewheight = CommonUtil.Px2Dp(this,
		// displayManager.getDisplayHeight(this) * 3 / 5);
		// view.setLayoutParams(new LayoutParams(viewwidth, viewheight));
		setContentView(R.layout.activity_login);

		TextView tv = (TextView) findViewById(R.id.title_text);
		tv.setText(R.string.login);
		etUserName = (EditText) findViewById(R.id.etUserName);
		etPassWord = (EditText) findViewById(R.id.etPassWord);
		btnLogin = (Button) findViewById(R.id.btnLogin);
		btnRButton = (Button) findViewById(R.id.btnRegister);

		btnLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String userName = etUserName.getText().toString();
				String passWord = etPassWord.getText().toString();
				System.out.println("2->" + userName + ">" + passWord);
				login(userName, passWord);
				finish();
			}
		});
		btnRButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(LoginActivity.this, RegisterActivity.class);
				startActivity(intent);
				finish();
			}
		});

	}

	private void login(final String userName, final String passWord) {
		new Thread() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					String result = HttpUtil.getRequest(MyConstants.NewURL
							+ "LoginActionphoneLogin?userName=" + userName
							+ "&userPassword=" + passWord);
					if (result != null) {
						user = GsonUtil.getGson().fromJson(result, User.class);
						Globe.user = user;
						// Toast.makeText(LoginActivity.this,
						// user.getName()+"��ӭ������",
						// Toast.LENGTH_SHORT).show();
						Intent intent = new Intent();
						intent.setClass(LoginActivity.this, Globe.className);
						startActivity(intent);
					} else {
						// Toast toast = Toast.makeText(LoginActivity.this,
						// "�˺Ż��������",
						// Toast.LENGTH_SHORT);
						// toast.show();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					// Toast.makeText(LoginActivity.this, "�������Ӵ���",
					// Toast.LENGTH_SHORT).show();
				}
			}
		}.start();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(LoginActivity.this, MainActivity.class);
			startActivity(intent);
			LoginActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
