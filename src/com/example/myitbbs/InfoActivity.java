package com.example.myitbbs;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dialog.LoginDialog;
import com.example.myitbbs.globe.Globe;

public class InfoActivity extends Activity {
	TextView titleText;
	ListView lvInfo;
	Button btn1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);

		// 设置标题
		titleText = (TextView) findViewById(R.id.title_text);
		titleText.setText(R.string.title_info);
		lvInfo = (ListView) findViewById(R.id.lvInfo);
		btn1 = (Button) findViewById(R.id.title_comment);
		btn1.setText("修改信息");
		btn1.setVisibility(View.VISIBLE);
		btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(InfoActivity.this, ReviseActivity.class);
				startActivity(intent);
			}
		});
		
		
		if (Globe.user != null) {
			MyBaseAdapter myBaseAdapter = new MyBaseAdapter();
			lvInfo.setAdapter(myBaseAdapter);
		} else {
			// Globe.className = InfoActivity.class;
			Globe.className = MainActivity.class;

			Toast toast = Toast.makeText(this, "请先登陆", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			Intent intent = new Intent();
			intent.setClass(InfoActivity.this, LoginDialog.class);
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_info, menu);
		return true;
	}

	class MyBaseAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return 11;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView textView = new TextView(InfoActivity.this);
			textView.setTextSize(20);
			textView.setTextColor(Color.BLACK);
			switch (position) {
			case 0:
				textView.setText("用户名：" + Globe.user.getName());
				break;
			case 1:
				textView.setText("QQ：" + Globe.user.getQq());
				break;
			case 2:
				textView.setText("Email：" + Globe.user.getEmail());
				break;
			case 3:
				textView.setText("Tel：" + Globe.user.getTel());
				break;
			case 4:
				textView.setText("真实姓名：" + Globe.user.getTruename());
				break;
			case 5:
				textView.setText("生日：" + Globe.user.getBirthday());
				break;
			case 6:
				textView.setText("性别：" + Globe.user.getSex());
				break;
			case 7:
				textView.setText("民族：" + Globe.user.getNation());
				break;
			case 8:
				textView.setText("专业：" + Globe.user.getMajor());
				break;
			case 9:
				textView.setText("入学时间：" + Globe.user.getEntranceYear());
				break;
			case 10:
				if (Globe.user.getPermission() == 4) {
					textView.setText("权限：管理员");
				} else {
					textView.setText("权限：普通用户");
				}
				break;
			default:
				break;
			}
			return textView;
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		if (Globe.user != null) {
			MyBaseAdapter myBaseAdapter = new MyBaseAdapter();
			lvInfo.setAdapter(myBaseAdapter);
		}
		super.onResume();

	}

}
