package com.example.myitbbs;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dialog.LoginDialog;
import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.globe.Globe;
import com.example.myitbbs.model.CategoryChild;
import com.example.myitbbs.util.GsonUtil;
import com.example.myitbbs.util.HttpUtil;
import com.google.gson.reflect.TypeToken;

public class PostActivity extends Activity {
	private Button btnPost;
	private EditText etTitle;
	private EditText etContents;
	private Myhandler myhandler;
	private Button btnBack;
	private List<CategoryChild> categoryList; // 分类列表
	private Handler handler; 
	private Spinner topicSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post);
		myhandler = new Myhandler();
		if(Globe.user == null){
			Toast toast = Toast.makeText(PostActivity.this,
					"请先登录", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			Intent intent = new Intent(PostActivity.this, LoginDialog.class);
			startActivity(intent);
			PostActivity.this.finish();
		}
		TextView tv = (TextView) findViewById(R.id.title_text);
		tv.setText("发帖");
		etTitle = (EditText) findViewById(R.id.etTitle);
		etContents = (EditText) findViewById(R.id.etNote);
		btnPost = (Button) findViewById(R.id.btnPost);
		btnPost.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String topicTitle = etTitle.getText().toString();
				String topicContents = etContents.getText().toString();
				if(topicContents == null || topicContents == ""){
					Toast toast = Toast.makeText(PostActivity.this,
							"发帖内容不能为空", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else if (topicTitle == null || topicTitle == ""){
					Toast toast = Toast.makeText(PostActivity.this,
							"发帖主题不能空", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else {
					int categoryChildId = categoryList.get(topicSpinner.getSelectedItemPosition()).getId();
					writeNewTopic(topicTitle, topicContents, categoryChildId);
				}
			}
		});
		btnBack = (Button) findViewById(R.id.title_back);
		btnBack.setVisibility(View.VISIBLE);
		btnBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PostActivity.this.finish();
			}
		});
		topicSpinner = (Spinner) findViewById(R.id.topicSpinner);
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 0x0002){
					MyBaseAdapter myBaseAdapter = new MyBaseAdapter();
					topicSpinner.setAdapter(myBaseAdapter);
				}
			}
		};
		updateData();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_post, menu);
		return true;
	}
	private void writeNewTopic(final String topicTitle, final String topicContents, 
			final int categoryChildId){
		new Thread(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Message msg = new Message();
				try {
					String result = HttpUtil.getRequest(MyConstants.NewURL +
							"UserActionphoneWriteNewTopic?phoneTopicTitle=" + topicTitle
							+ "&phoneTopictContents=" + topicContents + "&phoneCategoryChildId="
							+ categoryChildId + "&phoneUserId=" + Globe.user.getId());
					System.out.println("1->" + result);
					if(result.equals("#<postSuccess>\r\n")){
						msg.what = 1;
					} else {
						msg.what = 2;
					}
				} catch (Exception e) {
					// TODO: handle exception
					msg.what = 3;
					System.out.println("12");
				} finally {
					PostActivity.this.myhandler.sendMessage(msg);
				}
			}
		}.start();
	}
	class Myhandler extends Handler{
		public Myhandler(){}

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if(msg.what == 1){
				Toast toast = Toast.makeText(PostActivity.this,
						"发帖成功", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				Intent intent = new Intent(PostActivity.this, MainActivity.class);
				startActivity(intent);
			} else {
				Toast toast = Toast.makeText(PostActivity.this,
						"发帖失败", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
		}
	}
	class MyBaseAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return categoryList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView tv = new TextView(PostActivity.this);
			tv.setTextColor(getResources().getColor(R.color.black));
			tv.setTextSize(20);
			tv.setText(categoryList.get(position).getName());
			return tv;
		}
	}
	void updateData(){
		new Thread(){
			@Override
			public void run() {
				try {
					String result = HttpUtil.getRequest(MyConstants.URL + "phoneCategoryChildAll");
					if (result == null){
						return ;
					}
					categoryList = GsonUtil.getGson().fromJson(result, new TypeToken<List<CategoryChild>>() {
					}.getType());
					
					handler.sendEmptyMessage(0x0002);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};
		}.start();
	}
}
