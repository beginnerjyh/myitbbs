package com.example.myitbbs;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class MainActivity extends ActivityGroup {
	final int EXIT_DIALOG = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// 获取Tab的各个组件
		View firstTab = (View) LayoutInflater.from(this).inflate(
				R.layout.tabmini, null);
		TextView text0 = (TextView) firstTab.findViewById(R.id.tab_name);
		text0.setText("首页");
		ImageView imageView0 = (ImageView) firstTab.findViewById(R.id.tab_icon);
		imageView0.setBackgroundResource(R.drawable.icon_1_n);
		Intent intent1 = new Intent(this, FirstActivity.class);

		View chooseTab = (View) LayoutInflater.from(this).inflate(
				R.layout.tabmini, null);
		TextView text1 = (TextView) chooseTab.findViewById(R.id.tab_name);
		text1.setText("分类");
		ImageView imageView1 = (ImageView) chooseTab
				.findViewById(R.id.tab_icon);
		imageView1.setBackgroundResource(R.drawable.icon_2_n);
		Intent intent2 = new Intent(this, ChooseActivity.class);

		View myTab = (View) LayoutInflater.from(this).inflate(R.layout.tabmini,
				null);
		TextView text2 = (TextView) myTab.findViewById(R.id.tab_name);
		text2.setText("个人");
		ImageView imageView2 = (ImageView) myTab.findViewById(R.id.tab_icon);
		imageView2.setBackgroundResource(R.drawable.icon_4_n);
		Intent intent3 = new Intent(this, InfoActivity.class);

		View searchTab = (View) LayoutInflater.from(this).inflate(
				R.layout.tabmini, null);
		TextView text5 = (TextView) searchTab.findViewById(R.id.tab_name);
		text5.setText(R.string.search);
		ImageView imageView5 = (ImageView) searchTab
				.findViewById(R.id.tab_icon);
		imageView5.setBackgroundResource(R.drawable.icon_5_n);
		Intent intent5 = new Intent(this, SearchActivity.class);

		View moreTab = (View) LayoutInflater.from(this).inflate(
				R.layout.tabmini, null);
		TextView text3 = (TextView) moreTab.findViewById(R.id.tab_name);
		text3.setText("更多");
		ImageView imageView3 = (ImageView) moreTab.findViewById(R.id.tab_icon);
		imageView3.setBackgroundResource(R.drawable.more_page_btn_2);
		Intent intent4 = new Intent(this, MoreActivity.class);

		// 启动TabHost
		TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
		tabHost.setup(this.getLocalActivityManager()); // Call setup() before
														// adding tabs if
														// loading TabHost using
														// findViewById().

		// 绑定TabHost
		tabHost.addTab(tabHost.newTabSpec("firstTab").setIndicator(firstTab)
				.setContent(intent1));
		tabHost.addTab(tabHost.newTabSpec("chooseTab").setIndicator(chooseTab)
				.setContent(intent2));
		tabHost.addTab(tabHost.newTabSpec("myTab").setIndicator(myTab)
				.setContent(intent3));
		tabHost.addTab(tabHost.newTabSpec("searchTab").setIndicator(searchTab)
				.setContent(intent5));
		tabHost.addTab(tabHost.newTabSpec("moreTab").setIndicator(moreTab)
				.setContent(intent4));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		Dialog dialog = null;
		switch (id) {
		case EXIT_DIALOG:
			Builder bl = new AlertDialog.Builder(this);
			// bl.setIcon(R.drawable.header);
			bl.setTitle("提示");
			bl.setMessage("您确定要退出？");
			bl.setPositiveButton("确定", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					MainActivity.this.finish();
				}
			});
			bl.setNegativeButton("取消", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});
			dialog = bl.create();
			break;
		default:
			break;
		}
		return dialog;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			showDialog(EXIT_DIALOG);
		}
		return false;
	}

}
