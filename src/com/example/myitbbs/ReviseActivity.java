package com.example.myitbbs;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myitbbs.ReviseActivity.MyHandler;
import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.globe.Globe;
import com.example.myitbbs.model.User;
import com.example.myitbbs.util.HttpUtil;

public class ReviseActivity extends Activity{
	private EditText etName;
	private EditText etPassWord;
//	private EditText etPassWordToo;
	private EditText etTel;
	private EditText etSex;
	private EditText etmajor;
	private EditText ettruename;
	
	
	private Button btnRevise;
	
	
	String userName;
	String userPassWord;
//	String userPassWordToo;
	MyHandler myHandler;
	String userSex; 
	String userTel;
	String userMajor;
	String userTrueName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_revise);
		etName = (EditText) findViewById(R.id.etName);
		etPassWord = (EditText) findViewById(R.id.etPassWord);
		//etPassWordToo = (EditText) findViewById(R.id.etPassWordToo);
		etSex = (EditText) findViewById(R.id.etsex);
		etTel = (EditText) findViewById(R.id.etTel);
		ettruename =(EditText) findViewById(R.id.ettruename);
		etmajor = (EditText) findViewById(R.id.etmajor);
		
		btnRevise = (Button) findViewById(R.id.btnRevise);
		myHandler = new MyHandler();

		btnRevise.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				userPassWord = etPassWord.getText().toString();
			//	userPassWordToo = etPassWordToo.getText().toString();
				userTrueName = ettruename.getText().toString();
				userSex = etSex.getText().toString();
				userTel = etTel.getText().toString();
				userMajor = etmajor.getText().toString();
				
			/*if (!userPassWord.equals(userPassWordToo)) {
				//	Toast.makeText(ReviseActivity.this, "两次密码输入不一致",
					//		Toast.LENGTH_SHORT).show();
					etPassWord.setText("");
					etPassWordToo.setText("");
				}else*/
				if (userName == null || userName ==""){
					Toast.makeText(ReviseActivity.this, "请输入论坛账号",
							Toast.LENGTH_SHORT).show();
				}
				else if (userTrueName == null || userTrueName == "") {
					Toast.makeText(ReviseActivity.this, "请输入真实姓名",
							Toast.LENGTH_SHORT).show();
				}
				else if (userSex == null || userSex == "") {
					Toast.makeText(ReviseActivity.this, "请输入性别",
							Toast.LENGTH_SHORT).show();
				}
				else if(userTel == null || userTel == ""){
					Toast.makeText(ReviseActivity.this, "请输入电话",
							Toast.LENGTH_SHORT).show();
				}
				else if(userMajor ==null || userMajor ==""){
					Toast.makeText(ReviseActivity.this, "请输入专业",
							Toast.LENGTH_SHORT).show();
				}
				else {
					userName = etName.getText().toString();
					//revise(userName,userPassWordToo,userTrueName, userSex, userTel,userMajor);
					revise(userName,userPassWord,userTrueName,userSex,userTel,userMajor);
				}
			}
		});
	}

	private void revise(final String uesrName,final String userPassWord,final String userTrueName,final String userSex,final String userTel,final String userMajor) {
		new Thread() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Message msg = new Message();
				try {
					String result = HttpUtil.getRequest(MyConstants.NewURL
							+ "RegisterActionphoneRevise?userName="
							+ userName
							+"&userPassword=" + userPassWord
							+ "&userTel=" + userTel
							+ "&userTrueName=" + userTrueName
							+ "&userSex=" + userSex
							+ "&userMajor=" + userMajor
					);	
					if (result.equals("#<reviseSuccess>\r\n")) {//服务器发过来的消息会自动补上\r\n
						// 修改成功
						msg.what = 1;
						ReviseActivity.this.myHandler.sendMessage(msg);
					} else if (result.equals("#<reviseUnSuccess>\r\n")) {
						// 修改失败
						msg.what = 2;
						ReviseActivity.this.myHandler.sendMessage(msg);
					} else {
						// 发生网络错误
						msg.what = 3;
						ReviseActivity.this.myHandler.sendMessage(msg);
					}
				} catch (Exception e) {
					// TODO: handle exception
					msg.what = 3;
					ReviseActivity.this.myHandler.sendMessage(msg);
				}
			}
		}.start();
	}

	class MyHandler extends Handler {
		public MyHandler() {
		}

		public MyHandler(Looper L) {
			super(L);
		}

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			if (msg.what == 1) {
				User user = new User();
				user.setName(userName);
				user.setPassWord(userPassWord);
				user.setTruename(userTrueName);
				user.setSex(userSex);
				user.setTel(userTel);
				user.setMajor(userMajor);
				Globe.user = user;
				Toast.makeText(ReviseActivity.this, "修改成功",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent();
				intent.setClass(ReviseActivity.this, MainActivity.class);
				startActivity(intent);
			} else if (msg.what == 2) {
				Toast.makeText(ReviseActivity.this, "修改失败",
						Toast.LENGTH_SHORT).show();
				ReviseActivity.this.etPassWord.setText("");
			} else if (msg.what == 3) {
				Toast.makeText(ReviseActivity.this, "发生网络错误",
						Toast.LENGTH_SHORT).show();
			}
		}

	}
	
}
