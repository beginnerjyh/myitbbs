package com.example.myitbbs;

import com.example.myitbbs.http.AsyncHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class AbstractActivity extends Activity {
	public Context context;
	private ItbbsApplication application = null;
	public AsyncHttpClient client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		context = this;
		super.onCreate(savedInstanceState);
	}

	public void itbbsStartAforResult(Class<?> toactivity, Bundle bundle,
			int requestCode, boolean itfinish) {
		Intent intent = new Intent(this, toactivity);
		if (bundle != null) {
			intent.putExtras(bundle);
		}
		this.startActivityForResult(intent, requestCode);
		if (itfinish) {
			finish();
		}
	}

	public void itbbsStartA(Class<?> toactivity, Bundle bundle, boolean itfinish) {
		Intent intent = new Intent(this, toactivity);
		if (bundle != null) {
			intent.putExtras(bundle);
		}
		this.startActivity(intent);
		if (itfinish) {
			finish();
		}
	}

	/**
	 * ��ʼ���첽HTTP ����
	 */
	public void initHttpClient() {
		application = (ItbbsApplication) this.getApplication();
		client = application.getAsyncHttprequest();
	}

	public void T(String string) {
		Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
	}

	public void T(int resId) {
		T(context.getString(resId));
	}
}
