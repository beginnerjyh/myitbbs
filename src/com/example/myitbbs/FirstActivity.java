package com.example.myitbbs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.frame.first.NewsFragment;
import com.example.frame.first.PostFragment;

public class FirstActivity extends FragmentActivity {

	private ViewPager mPager;

	private Button framebtn_News_lastest;
	private Button framebtn_Post_lastest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_frist);
		framebtn_News_lastest = (Button) findViewById(R.id.frame_btn_news_lastest);
		framebtn_Post_lastest = (Button) findViewById(R.id.frame_btn_post_lastest);
		framebtn_News_lastest.setOnClickListener(new MyViewListener());
		framebtn_Post_lastest.setOnClickListener(new MyViewListener());
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				switch (arg0) {
				case 0:
					framebtn_News_lastest.setSelected(true);
					framebtn_Post_lastest.setSelected(false);
					break;
				case 1:
					framebtn_News_lastest.setSelected(false);
					framebtn_Post_lastest.setSelected(true);
					break;
				case 2:
					break;
				default:
					break;
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
		mPager.setCurrentItem(0);
		framebtn_News_lastest.setSelected(true);
	}

	class MyViewListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.frame_btn_news_lastest:
				mPager.setCurrentItem(0);
				break;
			case R.id.frame_btn_post_lastest:
				mPager.setCurrentItem(1);
				break;
			default:
				break;
			}
		}

	}

	private FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(
			getSupportFragmentManager()) {

		@Override
		public Fragment getItem(int position) {
			Fragment result = null;
			switch (position) {
			case 0:
				result = new NewsFragment();//
				break;
			case 1:
				result = new PostFragment();//
				break;
			default:

				break;
			}
			return result;
		}

		@Override
		public int getCount() {
			return 2;
		}
	};

}
