package com.example.myitbbs;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.model.Category;
import com.example.myitbbs.model.CategoryChild;
import com.example.myitbbs.model.User;
import com.example.myitbbs.util.GsonUtil;
import com.example.myitbbs.util.HttpUtil;
import com.google.gson.reflect.TypeToken;

public class ChooseActivity extends Activity {
	private TextView titleText;
	private List<Category> categoryList; // 分类列表
	private List<User> userList;  // 好友列表
	private Handler myHandler; 
	private ListView listView;
	private Button btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose);
		
		// 获取组件
		listView = (ListView) findViewById(R.id.choose_list);
		btn = (Button) findViewById(R.id.title_comment);
		btn.setText("发帖");
		btn.setVisibility(View.VISIBLE);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ChooseActivity.this, PostActivity.class);
				startActivity(intent);
			}
		});
		
		// 设置标题
		titleText = (TextView) findViewById(R.id.title_text);
		titleText.setText(R.string.title_choose);
		
		// 刷新请求
		myHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 0x0002){
					listView.setAdapter(new MyBaseAdapter());
					listView.setOnItemClickListener(new MyOnItemClickListener());
				}
			}
		};
		updateData();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_choose, menu);
		return true;
	}
	
	// 更新分类数据
	void updateData(){
		new Thread(){
			@Override
			public void run() {
				try {
					String result = HttpUtil.getRequest(MyConstants.URL + "phoneCategoryAll");
					if (result == null){
						return ;
					}
					categoryList = GsonUtil.getGson().fromJson(result, new TypeToken<List<Category>>() {
					}.getType());
					
					myHandler.sendEmptyMessage(0x0002);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};
		}.start();
	}
	
	class MyBaseAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 2 + categoryList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return categoryList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(ChooseActivity.this, R.layout.list_content, null);
			if (position == 0){
				TextView title = (TextView) view.findViewById(R.id.list_title);
				title.setText("新闻");
				TextView info = (TextView) view.findViewById(R.id.list_author);
				info.setText("这里有IT家园中最新最官方的新闻通知信息");
			}
			else if (position == categoryList.size() + 1) { // 判别是否是最后一个
				TextView title = (TextView) view.findViewById(R.id.list_title);
				title.setText("好友推荐");
			}
			else {
				TextView title = (TextView) view.findViewById(R.id.list_title);
				title.setText(categoryList.get(position - 1).getName());
				TextView info = (TextView) view.findViewById(R.id.list_author);
				info.setText(categoryList.get(position - 1).getDescribe());
			}
			return view;
		}
		
	}
	
	class MyOnItemClickListener implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			Intent intent = new Intent(ChooseActivity.this, ListActivity.class);
			if (position == 0){
				intent.putExtra("categoryId", 0);
				intent.putExtra("titleName", "it家园新闻");
			} else if (position == categoryList.size() + 1) {
				intent.putExtra("categoryId", -1);
				intent.putExtra("titleName", "好友推荐");
			}
			else { 
				intent.putExtra("categoryId", categoryList.get(position - 1).getId());
				intent.putExtra("titleName", categoryList.get(position - 1).getName());
			}
			startActivity(intent);
		}
		
	}

}
