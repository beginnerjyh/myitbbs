package com.example.myitbbs.model;


public class Letter {
	private int id;
	private String title;
	private String content;
	private User sender;
	private User receiver;
	private boolean isView;		


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	
	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}
	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		System.out.println(receiver);
		this.receiver = receiver;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public boolean isView() {
		return isView;
	}
}
