package com.example.myitbbs.model;

import java.util.Date;



public class Topic {
	private int id;
	private String title;
	private String contents;
	private String date;
	private User user;
	private String top;

	public String getTop() {
		return top;
	}

	public void setTop(String top) {
		this.top = top;
	}

	private CategoryChild categoryChild;
	
	public CategoryChild getCategoryChild() {
		return categoryChild;
	}

	public void setCategoryChild(CategoryChild categoryChild) {
		this.categoryChild = categoryChild;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

}
