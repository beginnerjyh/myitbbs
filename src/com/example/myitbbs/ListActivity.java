package com.example.myitbbs;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.globe.Globe;
import com.example.myitbbs.model.News;
import com.example.myitbbs.model.Topic;
import com.example.myitbbs.model.User;
import com.example.myitbbs.util.GsonUtil;
import com.example.myitbbs.util.HttpUtil;
import com.example.ui.MyListView;
import com.example.ui.MyListView.OnRefreshListener;
import com.google.gson.reflect.TypeToken;

public class ListActivity extends Activity {
	private MyListView listView;
	private int topicPageNum = 1;
	private int newsPageNum = 1;
	private int categoryId;
	private List<News> ltNews;
	private List<Topic> ltTopic;
	private List<User> ltFriend;
	private MyHandler myHandler;
	private Button btnBack;
	private TextView tvTitle;
	private Button btnPost;
	private List<User> li;
	Handler handler = new Handler();
	MyBaseAdapterForNews myBaseAdapterForNews;
	MyBaseAdapterForTopic myBaseAdapterForTopic;
	MyBaseAdapterForFriend myBaseAdapaterForFriend;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		listView = (MyListView) findViewById(R.id.info_list);
		btnBack = (Button) findViewById(R.id.title_back);
		tvTitle = (TextView) findViewById(R.id.title_text);
		btnBack.setVisibility(View.VISIBLE);
		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ListActivity.this.finish();
			}
		});
		btnPost = (Button) findViewById(R.id.title_comment);
		btnPost.setText("发帖");
		btnPost.setVisibility(View.VISIBLE);
		btnPost.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ListActivity.this, PostActivity.class);
				startActivity(intent);
			}
		});

		myHandler = new MyHandler();

		// 更新数据
		categoryId = getIntent().getExtras().getInt("categoryId");
		updateDate(categoryId);
		listView.setonRefreshListener(new OnRefreshListener() {
			public void onRefresh() {
				new AsyncTask<Void, Void, Void>() {
					protected Void doInBackground(Void... params) {
						try {
							categoryId = getIntent().getExtras().getInt("categoryId");
							updateDate(categoryId);
						} catch (Exception e) {
							e.printStackTrace();
						}
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						if(ltNews != null){
							myBaseAdapterForNews.notifyDataSetChanged();
						} else if (ltTopic != null) {
							myBaseAdapterForTopic.notifyDataSetChanged();
						}
						listView.onRefreshComplete();
					}

				}.execute();
			}
		});
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_list, menu);
		return true;
	}

	void updateDate(final int id) {
		new Thread() {
			Message msg = new Message();

			@Override
			public void run() {
				try {
					if (id == 0) {
						String result = HttpUtil.getRequest(MyConstants.URL
								+ "phoneNewsList");
						System.out.println(id + "<>" + result);
						ltNews = GsonUtil.getGson().fromJson(result,
								new TypeToken<List<News>>() {
								}.getType());
						msg.what = 1;
					}
					
					else if(id == -1){
						String result = HttpUtil.getRequest(MyConstants.URL
								+"phoneFriend");
						System.out.println(id + "<>" + result);
						ltFriend = GsonUtil.getGson().fromJson(result,
								new TypeToken<List<User>>(){
								}.getType());
						msg.what = 3;
					}
					 else {
						String result = HttpUtil.getRequest(MyConstants.URL
								+ "phoneChildTopic?categoryId=" + id
								+ "&topicPageNum=" + topicPageNum);
						ltTopic = GsonUtil.getGson().fromJson(result,
								new TypeToken<List<Topic>>() {
								}.getType());
						msg.what = 2;
					}
					

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					ListActivity.this.myHandler.sendMessage(msg);
				}
			};
		}.start();
	}

	class MyHandler extends Handler {
		public MyHandler() {

		}
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			tvTitle.setText(getIntent().getStringExtra("titleName"));
			if (msg.what == 1) {
				myBaseAdapterForNews = new MyBaseAdapterForNews();
				listView.setAdapter(myBaseAdapterForNews);
			}		
			else if(msg.what ==3){
				myBaseAdapaterForFriend = new MyBaseAdapterForFriend();
				listView.setAdapter(myBaseAdapaterForFriend);
				
			}
			 else {	
				if(ltTopic != null){
					myBaseAdapterForTopic = new MyBaseAdapterForTopic();
					listView.setAdapter(myBaseAdapterForTopic);
				}
			}
			listView.setOnItemClickListener(new MyOnItemClickListener());
		}

	}

	class MyBaseAdapterForTopic extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return ltTopic.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return ltTopic.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(ListActivity.this, R.layout.list_content,
					null);
			TextView title = (TextView) view.findViewById(R.id.list_title);
			title.setText(ltTopic.get(position).getTitle());
			TextView time = (TextView) view.findViewById(R.id.list_time);
			time.setText("发布时间：" + ltTopic.get(position).getDate().toString());
			return view;
		}

	}

	class MyBaseAdapterForNews extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return ltNews.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return ltNews.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(ListActivity.this, R.layout.list_content,
					null);
			TextView title = (TextView) view.findViewById(R.id.list_title);
			title.setText(ltNews.get(position).getName());
			TextView time = (TextView) view.findViewById(R.id.list_time);
			time.setText("发布时间：" + ltNews.get(position).getDate().toString());
			return view;
		}

	}

	
	class MyBaseAdapterForFriend extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return  ltFriend.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return ltFriend.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(ListActivity.this, R.layout.list_content, null);
			TextView title = (TextView) view.findViewById(R.id.list_title);
			title.setText(ltFriend.get(position).getName());
			
			return view;
		}
		
	}
	
	
	class MyOnItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, final int position,
				long arg3) {
			// 跳转定义
			Intent intent = new Intent(ListActivity.this, ContentActivity.class);
			
			if (ltNews != null) {
				intent.putExtra("title", ltNews.get(position-1).getName());
				intent.putExtra("time", "发布时间："
						+ ltNews.get(position-1).getDate());
				intent.putExtra("content", ltNews.get(position-1)
						.getContent());
				intent.putExtra("type", getString(R.string.title_news));
				startActivity(intent);
			} else if (ltTopic != null) {
				intent.putExtra("title", ltTopic.get(position-1)
						.getTitle());
				intent.putExtra("time", "发布时间：" + ltTopic.get(position-1).getDate());
				intent.putExtra("content",
						ltTopic.get(position-1).getContents());
				intent.putExtra("author", "作者：" + ltTopic.get(position-1).getUser().getName());
				intent.putExtra("type", getString(R.string.title_topic));
				intent.putExtra("topicId",ltTopic.get(position-1).getId());
				startActivity(intent);
			}
			else if(ltFriend != null)
			{
				User user = ltFriend.get(position - 1);  // 获取点击 的好友信息
				AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);
				builder.setTitle("好友信息");
				builder.setMessage("用户名：" + user.getName() + "\n" + "QQ：" + user.getQq() + "\n" +"Email" + user.getEmail() + "\n" +"Tel" + user.getTel() + "\n" +"真实姓名" + user.getTruename() + "\n" +"生日" + user.getBirthday() + "\n" +"性别" + user.getSex() + "\n" +"民族" + user.getNation() + "\n" +"专业" + user.getMajor() + "\n" +"入学时间" + user.getEntranceYear() +"\n");
				builder.setPositiveButton("加为好友", new android.content.DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						try {
							new Thread(){
								String result;
								public void run()
								{	
									try {
											result = HttpUtil.getRequest(MyConstants.NewURL +"UserActionphoneMakeFriend?uId=" + Globe.user.getId() + "&friendId=" + ltFriend.get(position).getId());
									}
									catch (Exception e)
									{
									// TODO Auto-generated catch block
										e.printStackTrace();
									}
									if (result != null && !result.equals("false"))
									{
										if("false".equals(result))
										{
											handler.post(new Runnable()
											{
												@Override
												public void run() 
												{
													Toast.makeText(getBaseContext(), "不能添加自己或已添加的好友", Toast.LENGTH_SHORT).show();
												}	
											});
										}
									//	
									}
									else 
									{
										handler.post(new Runnable()
										{
											@Override
											public void run()
											{
													Toast.makeText(getBaseContext(), "成功", Toast.LENGTH_SHORT).show();
											}
										});
									}
								};
							}.start();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				});
				builder.setNegativeButton("取消", null);
				builder.create().show();
				
			}
		}
	}
	

}
