package com.example.myitbbs;

import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

public class WelcomeActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);

		ImageView flag = (ImageView) findViewById(R.id.logo);
		AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(3000);
		anim.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {

			}

			@Override
			public void onAnimationRepeat(Animation arg0) {

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				itbbsStartA(MainActivity.class, null, true);
			}
		});
		flag.setAnimation(anim);
		anim.start();
	}
}
