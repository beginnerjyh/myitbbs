package com.example.myitbbs;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.model.News;
import com.example.myitbbs.model.Topic;
import com.example.myitbbs.util.GsonUtil;
import com.example.myitbbs.util.HttpUtil;
import com.google.gson.reflect.TypeToken;

public class SearchActivity extends Activity {

	private Button btnSearch;
	private EditText etSearch;
	private Handler myHandler;
	private ListView lvSearch;
	private List<Topic> topicList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		TextView tv = (TextView) findViewById(R.id.title_text);
		tv.setText(R.string.search);
		etSearch = (EditText) findViewById(R.id.etSearch);
		lvSearch = (ListView) findViewById(R.id.lvSearch);
		btnSearch = (Button) findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				search(etSearch.getText().toString());
			}
		});
		myHandler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				if(msg.what == 1){
					lvSearch.setAdapter(new BaseAdapter() {
						
						@Override
						public View getView(int position, View convertView, ViewGroup parent) {
							// TODO Auto-generated method stub
							View view = View.inflate(SearchActivity.this, R.layout.list_content, null);
							TextView title = (TextView) view.findViewById(R.id.list_title);
							title.setText(topicList.get(position).getTitle());
							TextView time = (TextView) view.findViewById(R.id.list_time);
							time.setText("发布时间：" + topicList.get(position).getDate().toString());
							TextView author = (TextView) view.findViewById(R.id.list_author);
							author.setText("作者：" + topicList.get(position).getUser().getName());	
							return view;
						}
						
						@Override
						public long getItemId(int position) {
							// TODO Auto-generated method stub
							return position;
						}
						
						@Override
						public Object getItem(int arg0) {
							// TODO Auto-generated method stub
							return null;
						}
						
						@Override
						public int getCount() {
							// TODO Auto-generated method stub
							return topicList.size();
						}
					});
					lvSearch.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(SearchActivity.this, ContentActivity.class);
							intent.putExtra("title", topicList.get(position)
									.getTitle());
							intent.putExtra("time", "发布时间：" + topicList.get(position).getDate());
							intent.putExtra("content",
									topicList.get(position).getContents());
							intent.putExtra("author", "作者：" + topicList.get(position).getUser().getName());
							intent.putExtra("type", getString(R.string.title_topic));
							intent.putExtra("topicId",topicList.get(position).getId());
							startActivity(intent);
						}
					});
				} else {
					Toast toast = Toast.makeText(SearchActivity.this,
							"没有搜索到相关内容", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
			
		};
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_search, menu);
		return true;
	}
	
	void search(final String word){
		new Thread(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Message msg = new Message();
				//获取查询结果
				try {
					String result = HttpUtil.getRequest(MyConstants.NewURL+"SearchActionabout?key="
				+ word);
					System.out.println(result);
					topicList = GsonUtil.getGson().fromJson(result,
							new TypeToken<List<Topic>>() {
							}.getType());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(topicList != null && topicList.size() != 0){
					msg.what = 1;
				} else {
					msg.what = 2;
				}
				SearchActivity.this.myHandler.sendMessage(msg);
			}
		}.start();
	}

}
