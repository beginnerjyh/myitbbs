package com.example.dialog;

import android.R.id;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.myitbbs.R;
import com.example.myitbbs.RegisterActivity;
import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.globe.Globe;
import com.example.myitbbs.model.User;
import com.example.myitbbs.util.GsonUtil;
import com.example.myitbbs.util.HttpUtil;
import com.example.myitbbs.util.StringUtils;

/**
 * �û���¼�Ի���
 * 
 * @version 1.0
 */
public class LoginDialog extends Activity {

	private ViewSwitcher mViewSwitcher;
	private ImageButton btn_close;
	private Button btn_login;
	private Button btn_regit;
	private AutoCompleteTextView mAccount;
	private EditText mPwd;
	private InputMethodManager imm;
	private User user = null;
	public final static int LOGIN_OTHER = 0x00;
	public final static int LOGIN_MAIN = 0x01;
	public final static int LOGIN_SETTING = 0x02;
	private Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_dialog);
		context = LoginDialog.this;
		imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

		mViewSwitcher = (ViewSwitcher) findViewById(R.id.logindialog_view_switcher);
		mAccount = (AutoCompleteTextView) findViewById(R.id.login_account);
		mPwd = (EditText) findViewById(R.id.login_password);

		btn_close = (ImageButton) findViewById(R.id.login_close_button);
		btn_close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LoginDialog.this.finish();
			}
		});
		btn_regit = (Button) findViewById(R.id.regist_btn_regist);
		btn_regit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, RegisterActivity.class);
				startActivity(intent);
				LoginDialog.this.finish();
			}
		});
		btn_login = (Button) findViewById(R.id.login_btn_login);
		btn_login.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// ����������
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

				String account = mAccount.getText().toString();
				String pwd = mPwd.getText().toString();
				// �ж�����
				if (StringUtils.isEmpty(account)) {
					Toast.makeText(context, "������˺Ų���Ϊ��", Toast.LENGTH_SHORT)
							.show();
					return;
				}
				if (StringUtils.isEmpty(pwd)) {
					Toast.makeText(context, "��������벻��Ϊ��", Toast.LENGTH_SHORT)
							.show();
					return;
				}

				btn_close.setVisibility(View.GONE);
				mViewSwitcher.showNext();

				login(account, pwd);
			}
		});

	}

	// ��¼��֤
	private void login(final String account, final String pwd,
			final boolean isRememberMe) {
		String userName = mAccount.getText().toString();
		String passWord = mPwd.getText().toString();
		System.out.println("2->" + userName + ">" + passWord);
		login(userName, passWord);
		finish();

	}

	private void login(final String userName, final String passWord) {
		new Thread() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					String result = HttpUtil.getRequest(MyConstants.NewURL
							+ "LoginActionphoneLogin?userName=" + userName
							+ "&userPassword=" + passWord);
					if (result != null) {
						user = GsonUtil.getGson().fromJson(result, User.class);
						Globe.user = user;
						// Toast.makeText(LoginActivity.this,
						// user.getName()+"��ӭ������",
						// Toast.LENGTH_SHORT).show();
						Intent intent = new Intent();
						intent.setClass(LoginDialog.this, Globe.className);
						startActivity(intent);
					} else {
						// Toast toast = Toast.makeText(LoginActivity.this,
						// "�˺Ż��������",
						// Toast.LENGTH_SHORT);
						// toast.show();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					// Toast.makeText(LoginActivity.this, "�������Ӵ���",
					// Toast.LENGTH_SHORT).show();
				}
			}
		}.start();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			this.onDestroy();
		}
		return super.onKeyDown(keyCode, event);
	}
}
