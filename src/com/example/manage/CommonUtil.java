package com.example.manage;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;

/**
 * 
 * 
 * @author raowei
 * 
 */
public class CommonUtil {

	/**
	 * 
	 * 计算文件大小
	 * 
	 * @param size
	 * @return
	 */
	public static String TransfSize(long size) {
		String resultString = "";

		DecimalFormat df = new DecimalFormat("###.##");
		float f;
		if (size < 1024 * 1024) {
			f = (float) ((float) size / (float) 1024);
			resultString = (df.format(new Float(f).doubleValue()) + "KB");
		} else {
			f = (float) ((float) size / (float) (1024 * 1024));
			resultString = (df.format(new Float(f).doubleValue()) + "MB");
		}

		return resultString;

	}

	/**
	 * 
	 * 获取当前时间
	 * 
	 * @param size
	 * @return
	 */
	public static String TransfData(long milliseconds) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		if (milliseconds == 0) {
			return sdf.format(new Date().getTime());
		}
		String resultString = "";

		resultString = sdf.format(milliseconds);
		return resultString;
	}

	public static int Dp2Px(Context context, float dp) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}

	public static int Px2Dp(Context context, float px) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (px / scale + 0.5f);
	}
}
