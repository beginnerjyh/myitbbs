package com.example.manage;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * 
 * 屏幕参数
 * @author rw
 * 
 */
public class DisplayManager {
	private static DisplayManager instance = null;
	private int displayWidth = 0; //显示器宽度
	private int displayHeight = 0; //显示器高度

	private DisplayMetrics dm = null;

	public static DisplayManager getInstance() {
		if (instance == null) {
			synchronized (DisplayManager.class) {
				if (instance == null) {
					instance = new DisplayManager();
				}
			}
		}
		return instance;
	}

	private DisplayManager() {
	}

	private String init(Context context) {
		try {
			dm = new DisplayMetrics();
			WindowManager wm = (WindowManager) context
					.getSystemService(Context.WINDOW_SERVICE);
			wm.getDefaultDisplay().getMetrics(dm);
			displayWidth = dm.widthPixels;
			displayHeight = dm.heightPixels;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return displayWidth + "*" + displayHeight;
	}

	public void refreshWnH(Context context) {
		@SuppressWarnings("unused")
		String res = this.init(context);

	}

	private void refresh(Context context) {
		if (dm != null) {
			return;
		}
		@SuppressWarnings("unused")
		String res = this.init(context);

	}

	public int getDisplayWidth(Context context) {
		this.refresh(context);
		return displayWidth;
	}

	public int getDisplayHeight(Context context) {
		this.refresh(context);
		return displayHeight;
	}

	public DisplayMetrics getDisplayMetrics(Context context) {
		this.refresh(context);
		return dm;
	}

}
