package com.example.frame.first;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myitbbs.R;
import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.http.AsyncHttpResponseHandler;
import com.example.myitbbs.model.News;
import com.example.myitbbs.util.GsonUtil;
import com.example.ui.MyListView.OnRefreshListener;
import com.google.gson.reflect.TypeToken;

public class NewsFragment extends FragmentBase implements OnItemClickListener {
	private List<News> newsList;
	private MyBaseAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		initHttpClient();
		newsList = new ArrayList<News>();
		adapter = new MyBaseAdapter();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = super.onCreateView(inflater, container, savedInstanceState);
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(this);
		mListView.setonRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				refreshdata();
			}
		});
		refreshdata();
		return v;
	}

	private void refreshdata() {
		String url = MyConstants.URL + "phoneLateNews";
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				// TODO Auto-generated method stub
				newsList = GsonUtil.getGson().fromJson(content,
						new TypeToken<List<News>>() {
						}.getType());
				adapter.notifyDataSetChanged();
				super.onSuccess(content);
			}

			@Override
			protected void sendFailureMessage(Throwable e, String responseBody) {
				// TODO Auto-generated method stub

				super.sendFailureMessage(e, responseBody);
			}

		});
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub

	}

	class MyBaseAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return newsList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View
					.inflate(getActivity(), R.layout.list_content, null);
			TextView title = (TextView) view.findViewById(R.id.list_title);
			title.setText(newsList.get(position).getName());
			TextView time = (TextView) view.findViewById(R.id.list_time);
			time.setText("����ʱ�䣺" + newsList.get(position).getDate().toString());
			return view;

		}
	}
}