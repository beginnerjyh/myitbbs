package com.example.frame.first;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myitbbs.R;
import com.example.myitbbs.constants.MyConstants;
import com.example.myitbbs.http.AsyncHttpResponseHandler;
import com.example.myitbbs.model.News;
import com.example.myitbbs.model.Topic;
import com.example.myitbbs.util.GsonUtil;
import com.example.ui.MyListView.OnRefreshListener;
import com.google.gson.reflect.TypeToken;

public class PostFragment extends FragmentBase implements OnItemClickListener {
	private List<Topic> topicList;
	private MyBaseAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initHttpClient();
		topicList = new ArrayList<Topic>();
		adapter = new MyBaseAdapter();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = super.onCreateView(inflater, container, savedInstanceState);
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(this);

		mListView.setonRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				refreshdata();
			}
		});
		refreshdata();

		return v;
	}

	private void refreshdata() {
		String url = MyConstants.URL + "phoneLateTopic";
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				// TODO Auto-generated method stub
				topicList = GsonUtil.getGson().fromJson(content,
						new TypeToken<List<Topic>>() {
						}.getType());
				adapter.notifyDataSetChanged();
				super.onSuccess(content);
			}

			@Override
			protected void sendFailureMessage(Throwable e, String responseBody) {
				// TODO Auto-generated method stub

				super.sendFailureMessage(e, responseBody);
			}

		});
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub

	}

	class MyBaseAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return topicList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View
					.inflate(getActivity(), R.layout.list_content, null);
			TextView title = (TextView) view.findViewById(R.id.list_title);
			title.setText(topicList.get(position).getTitle());
			TextView time = (TextView) view.findViewById(R.id.list_time);
			time.setText("发布时间：" + topicList.get(position).getDate().toString());
			TextView author = (TextView) view.findViewById(R.id.list_author);
			author.setText("作者：" + topicList.get(position).getUser().getName());
			return view;

		}
	}
}