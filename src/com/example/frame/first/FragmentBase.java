package com.example.frame.first;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myitbbs.ItbbsApplication;
import com.example.myitbbs.R;
import com.example.myitbbs.http.AsyncHttpClient;
import com.example.ui.MyListView;

public class FragmentBase extends Fragment {
	private ItbbsApplication application = null;
	public AsyncHttpClient client;
	protected MyListView mListView;
	public Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frame_list, container, false);
		mListView = (MyListView) v.findViewById(R.id.first_list);
		return v;
	}

	/**
	 * ��ʼ���첽HTTP ����
	 */
	public void initHttpClient() {
		application = (ItbbsApplication) this.getActivity().getApplication();
		client = application.getAsyncHttprequest();
	}
}
