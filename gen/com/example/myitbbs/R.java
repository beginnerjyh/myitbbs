/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.myitbbs;

public final class R {
    public static final class anim {
        public static final int translucent_zoom_exit=0x7f040000;
        public static final int translucent_zoom_in=0x7f040001;
        public static final int translucent_zoom_out=0x7f040002;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int black=0x7f050004;
        public static final int blue=0x7f050002;
        public static final int darkgray=0x7f050006;
        public static final int frame_button_text_light=0x7f05000b;
        public static final int full_transparent=0x7f050009;
        public static final int gold=0x7f050008;
        public static final int gray=0x7f050005;
        public static final int green=0x7f050001;
        public static final int half_transparent=0x7f05000a;
        public static final int red=0x7f050000;
        public static final int transparent=0x7f050007;
        public static final int white=0x7f050003;
    }
    public static final class drawable {
        public static final int arrow=0x7f020000;
        public static final int bottombg=0x7f020001;
        public static final int card_tag_bg_blue=0x7f020002;
        public static final int clear_but_background=0x7f020003;
        public static final int frame_button_bg=0x7f020004;
        public static final int frame_button_cutline=0x7f020005;
        public static final int frame_button_n=0x7f020006;
        public static final int frame_button_p=0x7f020007;
        public static final int ic_launcher=0x7f020008;
        public static final int ic_pulltorefresh_arrow=0x7f020009;
        public static final int icon_1_n=0x7f02000a;
        public static final int icon_2_n=0x7f02000b;
        public static final int icon_4_n=0x7f02000c;
        public static final int icon_5_n=0x7f02000d;
        public static final int itbbs_icon=0x7f02000e;
        public static final int login_bg=0x7f02000f;
        public static final int login_btn_selector=0x7f020010;
        public static final int login_button=0x7f020011;
        public static final int login_button_press=0x7f020012;
        public static final int login_close_button=0x7f020013;
        public static final int login_close_button_nor=0x7f020014;
        public static final int login_close_button_over=0x7f020015;
        public static final int login_line=0x7f020016;
        public static final int login_loading_15=0x7f020017;
        public static final int login_user_table_bg=0x7f020018;
        public static final int more_page_btn_2=0x7f020019;
        public static final int myitbbs_bg=0x7f02001a;
        public static final int myitbbs_logo=0x7f02001b;
        public static final int product_title_bg=0x7f02001c;
        public static final int return_button=0x7f02001d;
        public static final int search_history_clear_btn_bg_normal=0x7f02001e;
        public static final int search_history_clear_btn_bg_press=0x7f02001f;
        public static final int search_tab_normal=0x7f020020;
        public static final int tab_bg=0x7f020021;
        public static final int tab_selector=0x7f020022;
        public static final int title_bg=0x7f020023;
        public static final int title_button_bg_nor=0x7f020024;
        public static final int title_button_whitebg_sel=0x7f020025;
        public static final int widget_button_bg=0x7f020026;
    }
    public static final class id {
        public static final int btnComment=0x7f090002;
        public static final int btnLogin=0x7f090011;
        public static final int btnPost=0x7f090019;
        public static final int btnRegister=0x7f090012;
        public static final int btnRevise=0x7f090021;
        public static final int btnSearch=0x7f09003e;
        public static final int choose_list=0x7f090000;
        public static final int content_author=0x7f090005;
        public static final int content_content=0x7f090007;
        public static final int content_time=0x7f090006;
        public static final int content_title=0x7f090004;
        public static final int etComment=0x7f090001;
        public static final int etEmail=0x7f09001c;
        public static final int etName=0x7f09001a;
        public static final int etNote=0x7f090018;
        public static final int etPassWord=0x7f090010;
        public static final int etPassWordToo=0x7f09001b;
        public static final int etSearch=0x7f09003d;
        public static final int etTel=0x7f09001f;
        public static final int etTitle=0x7f090017;
        public static final int etUserName=0x7f09000f;
        public static final int etmajor=0x7f090020;
        public static final int etsex=0x7f09001e;
        public static final int ettruename=0x7f09001d;
        public static final int first_list=0x7f090024;
        public static final int frame_btn_news_lastest=0x7f09000a;
        public static final int frame_btn_post_lastest=0x7f09000b;
        public static final int head_arrowImageView=0x7f090026;
        public static final int head_contentLayout=0x7f090025;
        public static final int head_lastUpdatedTextView=0x7f090029;
        public static final int head_progressBar=0x7f090027;
        public static final int head_tipsTextView=0x7f090028;
        public static final int include1=0x7f090008;
        public static final int info_list=0x7f09000e;
        public static final int linear1=0x7f090009;
        public static final int linearLayout_tab=0x7f090014;
        public static final int list_author=0x7f090030;
        public static final int list_time=0x7f090031;
        public static final int list_title=0x7f09002f;
        public static final int ll=0x7f090003;
        public static final int login_account=0x7f090037;
        public static final int login_btn_login=0x7f09003a;
        public static final int login_close_button=0x7f09003c;
        public static final int login_password=0x7f090038;
        public static final int login_scrollview=0x7f090033;
        public static final int login_uly=0x7f090036;
        public static final int login_user_table=0x7f090035;
        public static final int logindialog_space=0x7f090032;
        public static final int logindialog_view_switcher=0x7f090034;
        public static final int logintab=0x7f090039;
        public static final int logo=0x7f090023;
        public static final int lvInfo=0x7f09000d;
        public static final int lvMore=0x7f090015;
        public static final int lvSearch=0x7f090022;
        public static final int menu_quit=0x7f090044;
        public static final int menu_settings=0x7f090045;
        public static final int pager=0x7f09000c;
        public static final int pull_to_refresh_header=0x7f09002a;
        public static final int pull_to_refresh_image=0x7f09002c;
        public static final int pull_to_refresh_progress=0x7f09002b;
        public static final int pull_to_refresh_text=0x7f09002d;
        public static final int pull_to_refresh_updated_at=0x7f09002e;
        public static final int regist_btn_regist=0x7f09003b;
        public static final int tab_icon=0x7f09003f;
        public static final int tab_name=0x7f090040;
        public static final int tabhost=0x7f090013;
        public static final int title_back=0x7f090041;
        public static final int title_comment=0x7f090043;
        public static final int title_text=0x7f090042;
        public static final int topicSpinner=0x7f090016;
    }
    public static final class layout {
        public static final int activity_choose=0x7f030000;
        public static final int activity_comment=0x7f030001;
        public static final int activity_content=0x7f030002;
        public static final int activity_frist=0x7f030003;
        public static final int activity_info=0x7f030004;
        public static final int activity_list=0x7f030005;
        public static final int activity_login=0x7f030006;
        public static final int activity_main=0x7f030007;
        public static final int activity_more=0x7f030008;
        public static final int activity_post=0x7f030009;
        public static final int activity_register=0x7f03000a;
        public static final int activity_revise=0x7f03000b;
        public static final int activity_search=0x7f03000c;
        public static final int activity_welcome=0x7f03000d;
        public static final int frame_list=0x7f03000e;
        public static final int head=0x7f03000f;
        public static final int headview=0x7f030010;
        public static final int list_content=0x7f030011;
        public static final int login_dialog=0x7f030012;
        public static final int search=0x7f030013;
        public static final int tabmini=0x7f030014;
        public static final int title=0x7f030015;
    }
    public static final class menu {
        public static final int activity_choose=0x7f080000;
        public static final int activity_comment=0x7f080001;
        public static final int activity_content=0x7f080002;
        public static final int activity_first=0x7f080003;
        public static final int activity_info=0x7f080004;
        public static final int activity_list=0x7f080005;
        public static final int activity_main=0x7f080006;
        public static final int activity_more=0x7f080007;
        public static final int activity_post=0x7f080008;
        public static final int activity_search=0x7f080009;
    }
    public static final class string {
        public static final int app_name=0x7f060000;
        public static final int hello_world=0x7f060001;
        public static final int login=0x7f06000c;
        public static final int menu_quit=0x7f060002;
        public static final int menu_settings=0x7f060015;
        public static final int menu_update=0x7f060003;
        public static final int putName=0x7f06000f;
        public static final int putPassWord=0x7f06000e;
        public static final int register=0x7f06000d;
        public static final int search=0x7f06001b;
        public static final int title_activity_choose=0x7f060011;
        public static final int title_activity_comment=0x7f060018;
        public static final int title_activity_content=0x7f060016;
        public static final int title_activity_first=0x7f060010;
        public static final int title_activity_info=0x7f060012;
        public static final int title_activity_list=0x7f060017;
        public static final int title_activity_main=0x7f060004;
        public static final int title_activity_more=0x7f060013;
        public static final int title_activity_post=0x7f060019;
        public static final int title_activity_revise=0x7f060014;
        public static final int title_activity_search=0x7f06001a;
        public static final int title_back=0x7f06000b;
        public static final int title_choose=0x7f060006;
        public static final int title_first=0x7f060005;
        public static final int title_info=0x7f060007;
        public static final int title_more=0x7f060008;
        public static final int title_news=0x7f06000a;
        public static final int title_topic=0x7f060009;
    }
    public static final class style {
        public static final int Animation=0x7f070003;
        public static final int Animation_Translucent=0x7f070002;
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.


    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.


        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070004;
        public static final int Theme_HalfTranslucent=0x7f070001;
        public static final int frame_button=0x7f070005;
    }
}
